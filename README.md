# DEPENDENCES

+ Cadaver: http://www.webdav.org/cadaver/
+ Telegram Notify: https://github.com/rakshazi/telegram-notify


# HOW TO INSTALL

```
git clone https://gitlab.com/zagur/backupvpscloud.git
cd backupvpscloud
chmod 755 backupvpscloud.sh
./backupvpscloud.sh -i
```
You can modify parameters on /etc/backupvpscloud.cfg

# HOW TO REMOVE

```
backupvpscloud.sh -r
```

# UPDATE

```
backupvpscloud.sh -u
```
# OTHERS OPTIONS

Show version

```
backupvpscloud.sh -v
```

Show help

```
backupvpscloud.sh
```



