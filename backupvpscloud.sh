#!/bin/bash
# Jesus Camacho
# zagurblog@gmail.com
# 0.4.4 version

###################
# VARIABLES
###########################################

source /etc/bvpsc/backupvpscloud.cfg 2> /dev/null

## DON'T TOUCH!!
gitFileRaw="https://gitlab.com/t3rr0rz0n3/backupvpscloud/raw/master/backupvpscloud.sh"
gitRepo="https://gitlab.com/t3rr0rz0n3/backupvpscloud.git"
configFile="/etc/bvpsc/backupvpscloud.cfg"
############# Really, don't touch ¬.¬ ###

# FUNCTIONS

function installScript() {

        # If is equal 0, is not installed
        if [ `whereis backupvpscloud.sh | cut -d"/" -f3` != "backupvpscloud.sh" ]
        then
                echo "Installing..."
                chmod 700 $0
                chown root:root $0
                cp $0 /bin/
                checkConfigFile
                echo "Installation complete!"
                echo "$date - NOTICE: $0 copy in /bin" >> $logUpdate
        else
                echo "Deleting old version..."
                rm -f /bin/$0
                echo "Installing..."
                chmod 700 $0
                chown root:root $0
                cp $0 /bin/
                checkConfigFile
                echo "Installation complete!"
                echo "$date - NOTICE: UPDATE $0 copy in /bin" >> $logUpdate
        fi
}

function removeScript() {

        echo -n "Are you sure? y/n : "
        read sureRemove

        if [ $sureRemove == "y" ]
        then

                # Remove config file
                if [ -d /etc/bvpsc ]
                then
                        if [ -f $configFile ]
                        then
                                echo -n "Do you want remove file config? (y/n): "
                                read remove

                                if [ "$remove" == "y" ]
                                then
                                        rm -rf /etc/bvpsc
                                fi
                        fi
                fi

                #Remove .sh
                if [ -f $0 ]
                then
                        rm -f $0
                fi
                echo "$0 deleted..."
        else
                echo "Abort desinstalation"
        fi
}

function checkConfigFile() {

        if [ -d /etc/bvpsc ]
        then
                if [ -f $configFile ]
                then
                        echo -n "File $configFile exits. Do you want to replace it? (y/n): "
                        read replace

                        if [ "$replace" == "y" ]
                        then
                                cp backupvpscloud.cfg /etc/bvpsc/
                        fi
                fi
        else
                mkdir -p /etc/bvpsc
                cp backupvpscloud.cfg /etc/bvpsc/
        fi
}

function installPackage() {
        apt install $1 -y >> /dev/null
}

function updatePackage() {

        if isPackageInstalled bc
        then
                oldVersion=`head -5 /bin/backupvpscloud.sh | grep "version" | cut -d" " -f2`
                newVersion=`curl --silent $gitFileRaw 2> /dev/null | head -5 | grep version | cut -d" " -f2`
                if (( $(echo "$oldVersion == $newVersion" | sed 's/[\._-]//g' | bc -l) ))
                then
                        # Same version
                        echo "Server Version:           $oldVersion"
                        echo "Repository version:       $newVersion"
                        echo "----------------------- Nothing to do"
                elif (( $(echo "$oldVersion < $newVersion" | sed 's/[\._-]//g' | bc -l) ))
                then
                        # Diff version, update
                        echo "Server Version:           $oldVersion"
                        echo "Repository Version:       $newVersion"
                        echo "------------------------- Updating..."
                        git clone $gitRepo /tmp/backupvpscloud
                        chmod 755 /tmp/backupvpscloud/backupvpscloud.sh
                        bash /tmp/backupvpscloud/backupvpscloud.sh -i
                        rm -rf /tmp/backupvps/cloud
                        echo "$date - UPDATE: $0 ha passat de $oldVersion a $newVersion" >> $logUpdate
                        echo "OK..."
                else
                        echo "
                        This version is old. You can download the last version here:
                        $gitRepo
                        "
                fi
        else
                echo "$1 is not installed"
                installPackage bc
                updatePackage
        fi
}

function isPackageInstalled() {

	command -v $1 > /dev/null
        if [ $? == "0" ]
        then
		return 0
        else
                return 1
        fi
}

function isOptionEmpty() {

	if [ -z $1 ]
	then
		return 1
	else
		return 0
	fi
}

function isDirExists() {

        if [ -d $1 ]
        then
                return 0
        else
                return 1
        fi
}

function addToLog() {

        # Check exit status
        if [ $? == 0 ]
        then
                echo "$dateLog - OK - $1" >> $logFile
        else
                echo "$dateLog - FAIL - $1" >> $logFile
        fi
}

function toTarFile() {

        if isDirExists $tempFolder
        then
		if isOptionEmpty $pathToApacheWeb
		then
			if [ "$type" == "domain" ]
			then
				tar zcfP $tempFolder/$1-$date.tar.gz $pathToApacheWeb/$1/www/ >> /dev/null
			fi

			if [ "$type" == "path" ]
			then
				fileTar=`echo $1 | tr "/" "_"`
				tar zcfP $tempFolder/$fileTar-$date.tar.gz $1 >> /dev/null
			fi

		else
			addToLog "pathToApacheWeb is empty"
			showError "toTarFile" "pathToApacheWeb is empty"
			exit 1
		fi
	else
		addToLog "$tempFolder not found"
		showError "toTarFile" "$tempFOlder not found"
		exit 1
	fi
}

function toExportAllDB() {

	if isOptionEmpty $1
	then
		if [ "$namedb" == "all" ]
		then
			echo  "$userdb -p$passwddb --all-databases > $tempFolder/all-databases-$date.sql"
			mysqldump -u $userdb -p$passwddb --all-databases > $tempFolder/all-databases-$date.sql
			addToLog "File all-databases-$date.sql upload to $urlServer"
		fi
	else
		addToLog "$namedb is empty"
		showError "toExportDB" "$namedb is empty"
	fi
}

function showVersion() {

        actualVersion=`head -5 /bin/backupvpscloud.sh | grep "version" | cut -d" " -f2`
        echo "@ BackupVPScloud by Jesús Camacho | @t3rr0rz0n3"
        echo "Server Version:   $actualVersion"
        echo ""
}


function showError() {

        echo "ERROR: Function $1(): $2"
}

function showHelp() {
	clear
        echo $1
        echo "
        Usage: backpvpscloud.sh [OPTION]
	"
        echo "
	Options:
        -h, -H, --help			Show help
        -u, -U, --update		Check if exist another version in repo
	-b, -B, --backup		Create backup
	-i, -I, --install		Install file to /bin
	-r, -R, --remove		Remove file in /bin
	-v, --version			Show version
	"
	echo "
	Path logs:
	Log Update: /var/log/backupvpscloud_update.log
	Log File: /var/log/backupvpscloud.log
	"
}

function uploadCloud() {


	if [ $toolUpload == "webdav" ]
	then

		if isPackageInstalled cadaver
		then
			# Check if server is alive
			if ! ping -c 1 -w 5 $urlServer &>/dev/null ;
			then
				addToLog "ERROR: $date - $urlServer is down!"
			else
				cadaver $urlNextCloud <<< "put $tempFolder/$1-$date.tar.gz" 2> /dev/null
				addToLog "File $1-$date.tar.gz upload to $urlServer"
			fi
		else
			showError "uploadCloud" "I require cadaver but it's not installed. Aborting."
                        showError "uploadCloud" "Try to install: apt-get install cadaver"
                        showError "uploadCloud" "And configure ~/.netrc"
                        addToLog "I require cadaver but it's not installed. Aborting. Try to install: apt-get install cadaver and configure ~/.netrc"
		fi

	elif [ $toolUpload == "s3cmd" ]
	then

		if isPackageInstalled s3cmd
		then
			if [ -f $s3cmdConfigFile ]
			then
				if [ "$type" == "domain" ]
				then
					s3cmd put --recursive $tempFolder/$1-$date.tar.gz $bucket/$bucketpath/$1-$date.tar.gz
					addToLog "File $1-$date.tar.gz upload to $bucket/$bucketpath"
				fi
				if [ "$type" == "path" ]
				then
					fileTar=`echo $1 | tr "/" "_"`
					s3cmd put --recursive $tempFolder/$fileTar-$date.tar.gz $bucket/$bucketpath/$fileTar-$date.tar.gz
                                        addToLog "File $1-$date.tar.gz upload to $bucket/$bucketpath"
				fi
				if [ "$type" == "dball"]
				then
					all-databases-2018-02-08_16:29:29.sql
				fi
			else
				addToLog "File $s3cmdConfigFile not found"
				showError "uploadCloud" "$s3cmdConfigFIle not defined"
			fi
		else
			showError "uploadCloud" "I require s3cmd but it's not installed. Aborting."
			showError "uploadCloud" "Try to install: apt-get install s3cmd"
			showError "uploadCloud" "And configure /root/.s3cfg"
			addToLog "I require s3cmd but it's not installed. Aborting. Try to install: apt-get install s3cmd and configure /root/.s3cfg"
		fi

	elif [ $toolUpload == "ftp" ]
	then
		echo "Se sube ftp"
	else
		addToLog "Option toolUpload in /etc/bvpsc/backupvpscloud.cfg not defined"
		showError "uploadCloud" "toolUpload in /etc/bvpsc/backupvpscloud.cfg not defined"
	fi
}

## PROGRAM
op=$1
case $1 in
	-b | -B | --backup)

		# Check if dir temporal exists
		if ! isDirExists $tempFolder
		then
			mkdir $tempFolder
		fi

		# Number of domains,paths and DB in config file + 1
		numsDomains=$(grep -o "$split" <<< "$urlDomainsInServer" | wc -l)
		numsDomains=$((numsDomains + 1))
		numsPaths=$(grep -o "$split" <<< "$otherPaths" | wc -l)
		numsPaths=$((numsPaths + 1))
		numsDB=$(grep -o "$split" <<< "$databaseName" | wc -l)
		numsDB=$((databaseName + 1))

		# Compress directories tar.gz by domain
		for i in `seq 1 $numsDomains`;
		do
			type="domain"
			fileToTarGz=`echo $urlDomainsInServer | cut -d"$split" -f$i`

			# Create tar.gz file
			toTarFile $fileToTarGz

			# Add to log
			addToLog "File compress: $fileToTarGz-$date.tar.gz"

			# Upload File s3cmd/cadaver/FTP
			uploadCloud $fileToTarGz
		done

		# Compress other directories
		for i in `seq 1 $numsPaths`;
		do
			type="path"
			fileToTarGz=`echo $otherPaths | cut -d"$split" -f$i`

			# Create tar.gz file
			toTarFile $fileToTarGz

			# Add to Log
			addToLog "File compress: $fileToTarGz-$date.tar.gz"

			# Upload others files s3cmd/cadaver/FTP
			uploadCloud $fileToTarGz
		done

		# DATABASES

		if isOptionEmpty $namedb
		then
			if [ "$namedb" == "all" ]
			then
				toExportAllDB $namedb
				addToLog "File compress: all-databases.sql"
				uploadCloud 
			else
				echo "MEH"
				#FOR BLABLA toExportDB
			fi
		else
			addToLog "$namedb is empty"
			showError "toExportDB" "$namedb is empty"
		fi


		# Remote temporal folder
		rm -rf $tempFolder
	;;

	-u | -U | --update)
		updatePackage
	;;

	-v | -version)
		showVersion
	;;
	-i | -I | --install)
		installScript
	;;

	-r | -R | --remove)
		removeScript
	;;

	-h | -H | --help)
		showHelp
		addToLog "Se ha pedido ayuda"
	;;

	*)
		showHelp
	;;
esac


# TO DO
# web files (WP, nuvol, piwik)
# bbdd (WP, nuvol, piwik)
# config apache files
# SSL letsencript

# Añadir a cron con -c o directamente en la instalación
# Informe e-mail
# LOG mirar de poner estados y si es compres o upload, etc
# Fecha - STADO - frase - fichero - 
# Añadir opcion -l para mostrar log y filtrar por estado, por servicio etc
